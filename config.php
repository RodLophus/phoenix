<?php

$config = array(
  'keyCloack' => array(
    'authServerUrl'       => 'https://autenticacaocentral.unicamp.br/auth',
    'realm'               => 'unicamp-auth',
    'clientId'            => 'PHOENIX_IMECC',
    'clientSecret'        => '94267d01-d8e0-4fde-a035-3912b694f78d',
    'redirectUri'         => 'https://servicos.ime.unicamp.br/phoenix/index.php',
    'encryptionAlgorithm' => 'RS256',
    'encryptionKeyPath'   => '/etc/ssl/certs/servicos.ime.unicamp.br.pem'
  ),
  'ldap' => array(
    'serverUri'           => 'ldap://ldap.ime.unicamp.br',
    'baseDn'              => 'dc=ime,dc=unicamp,dc=br',
    'uidSearchAttribute'  => 'siseUid',
    'uidAttribute'        => 'uid',
    'nameAttribute'       => 'cn',
    'emailAttribute'      => 'mail'
  ),
  'clientChannel' => array(
    'encryptionKeyPath'   => '/etc/ssl/private/servicos.ime.unicamp.br.key'
  ),
  'logoutRedirection' => array(
    'DEFAULT'  => 'https://servicos.ime.unicamp.br',
    'sasser'   => 'https://servicos.ime.unicamp.br/sasser'
  )
);

?>
