<?php

require_once('config.php');
require_once('vendor/autoload.php');

function getLdapUserBySiseUid($siseUid) {
    $config = $GLOBALS['config']['ldap'];
    $result = array('uid' => NULL, 'name' => NULL, 'email' => NULL);
    $ldapFilter = '(' . $config['uidSearchAttribute'] . '=' . $siseUid . ')';
    $ldap = ldap_connect($config['serverUri']);
    if($queryResult = ldap_search($ldap, $config['baseDn'], $ldapFilter, array($config['uidAttribute'], $config['nameAttribute'], $config['emailAttribute']))) {
        if($entry = ldap_first_entry($ldap, $queryResult)) {
            $result['uid']   = @ldap_get_values($ldap, $entry, $config['uidAttribute'])[0];
            $result['name']  = @ldap_get_values($ldap, $entry, $config['nameAttribute'])[0];
            $result['email'] = @ldap_get_values($ldap, $entry, $config['emailAttribute'])[0];
        }
    }
    return($result);
}


function sslEncrypt($data) {
    $encryptionKeyPath = $GLOBALS['config']['clientChannel']['encryptionKeyPath'];
    $privateKeyFile = fopen($encryptionKeyPath, 'r');
    if(! $privateKeyFile) {
        exit('Error reading private key.');
    }
    $privateKey = fread($privateKeyFile, 8192);
    fclose($privateKeyFile);
    $privateKey = openssl_get_privatekey($privateKey);
    if(! openssl_private_encrypt(json_encode($data), $result, $privateKey)) {
        exit('Unable to encrypt final assertion.');
    }
    return(base64_encode($result));
}



session_start();

if(empty($_SESSION['redirectUri']) && empty($_GET['redirect_uri'])) {
    session_unset();
    session_commit();
    exit('Invalid request.');
}

if(isset($_GET['redirect_uri'])) {
    $_SESSION['redirectUri'] = $_GET['redirect_uri'];
}

$provider = new Stevenmaguire\OAuth2\Client\Provider\Keycloak($config['keyCloack']);

if (!isset($_GET['code'])) {

    // If we don't have an authorization code then get one
    $authUrl = $provider->getAuthorizationUrl();
    $_SESSION['oauth2state'] = $provider->getState();
    header('Location: '.$authUrl);
    exit;

// Check given state against previously stored one to mitigate CSRF attack
} elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {

    unset($_SESSION['oauth2state']);
    exit('Invalid state, make sure HTTP sessions are enabled.');

} else {

    // Try to get an access token (using the authorization code grant)
    try {
        $token = $provider->getAccessToken('authorization_code', [
            'code' => $_GET['code']
        ]);
    } catch (Exception $e) {
        exit('Failed to get access token: '.$e->getMessage());
    }

    // Optional: Now you have a token you can look up a users profile data
    try {

        // We got an access token, let's now get the user's details
        $user = $provider->getResourceOwner($token);
        $user = $user->toArray();
        $data = array(
            'uec' => array(
                'uid'   => $user['preferred_username'],
                'email' => $user['email'],
		'name'  => $user['given_name']
            ),
            'imecc' => getLdapUserBySiseUid($user['preferred_username'])
        );
        header('Location: ' . $_SESSION['redirectUri'] . '/?token=' . urlencode(sslEncrypt($data)) . '&logout_url=' . urlencode($provider->getLogoutUrl()));
        session_destroy();
    } catch (Exception $e) {
        exit('Failed to get resource owner: '.$e->getMessage());
    }

}

?>
