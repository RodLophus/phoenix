<?php

require_once('config.php');

$site = $_GET['site'] ?? false;

$logoutRedirection = $GLOBALS['config']['logoutRedirection'];
$redirectUrl = $logoutRedirection['DEFAULT'] ?? '/';

if($site && isset($logoutRedirection[$site])) {
  $redirectUrl = $logoutRedirection[$site];
}

header("Location: $redirectUrl");

?>
